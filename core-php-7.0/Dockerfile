FROM php:7.0.30

LABEL maintainer="Michael Dyrynda <michael@dyrynda.com.au>"

ARG TIMEZONE=Australia/Adelaide
ARG LOCALE=en_AU.UTF-8

ENV MAX_INPUT_TIME=60 \
    MEMORY_LIMIT=128M \
    DISPLAY_ERRORS=Off \
    POST_MAX_SIZE=8M \
    HTML_ERRORS=On \
    DEFAULT_SOCKET_TIMEOUT=60 \
    MAX_EXECUTION_TIME=30 \
    UPLOAD_MAX_FILESIZE=2M \
    MAX_FILE_UPLOADS=20 \
    OPCACHE_ENABLE=0 \
    OPCACHE_MEMORY_CONSUMPTION=128 \
    OPCACHE_INTERNED_STRINGS_BUFFER=16 \
    OPCACHE_MAX_ACCELERATED_FILES=10000 \
    OPCACHE_MAX_WASTED_PERCENTAGE=5 \
    OPCACHE_USE_CWD=1 \
    OPCACHE_VALIDATE_TIMESTAMPS=0 \
    OPCACHE_REVALIDATE_FREQ=0 \
    OPCACHE_REVALIDATE_PATH=0 \
    OPCACHE_SAVE_COMMENTS=0 \
    OPCACHE_FAST_SHUTDOWN=0

RUN apt-get update && \
    apt-get install -y mysql-client git zlib1g-dev locales openssh-client > /dev/null && \
    docker-php-ext-install zip bcmath pdo_mysql mysqli > /dev/null

RUN echo "en_AU.UTF-8 UTF-8" >> /etc/locale.gen && locale-gen && \
    echo "LANG=\"$LOCALE\"" >> /etc/default/locale && \
    echo "LANG=\"$LOCALE\"" >> /etc/profile && \
    echo "LC_MONETARY=\"$LOCALE\"" >> /etc/profile && \
    echo "$TIMEZONE" > /etc/timezone

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer

RUN php -r "unlink('composer-setup.php');"

ADD php.ini /etc/php
